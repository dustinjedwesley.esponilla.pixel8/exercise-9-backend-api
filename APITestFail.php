<?php

    require_once 'API.php';
    use PHPUnit\Framework\TestCase;

    class APITestFail extends TestCase
    {
        private $api;

        protected function setUp(): void
        {
            $this->api = new API();
        }
        
        public function testHttpPost()
        {
            $_SERVER['REQUEST_METHOD'] = 'POST';

            $payload = array();

            $result = json_decode($this->api->httpPost($payload), true);
            $this->assertArrayHasKey('status', $result);
            $this->assertEquals($result['status'], 'failed');

            try {
                $id = $result['data'][0]['id'];
            } catch (Exception $e) {
                $id = NULL;
            }
            return $id;
        }

        /**
         * @depends testHttpPost
         */
        public function testHttpGet($id) 
        {
            $_SERVER['REQUEST_METHOD'] = 'GET';
            $payload = $id;

            $result = json_decode($this->api->httpGet($payload), true);
            $this->assertArrayHasKey('status', $result);
            $this->assertEquals($result['status'], 'failed');
        }

        /**
         * @depends testHttpPost
         */
        public function testHttpPut($id) 
        {
            $_SERVER['REQUEST_METHOD'] = 'PUT';

            $givenID = $id;
            $payload = array(
                'id' => $id, 
                'first_name' => 'Dustin Testing Put',
                'middle_name' => 'Put',
                'last_name' => 'Esponilla',
                'contact_number' => '09123456789'
            );

            $result = json_decode($this->api->httpPut($givenID, $payload), true);
            $this->assertArrayHasKey('status', $result);
            $this->assertEquals($result['status'], 'failed'); 
        }

        /**
         * @depends testHttpPost
         */
        public function testHttpDelete($id) 
        {
            $_SERVER['REQUEST_METHOD'] = 'DELETE';

            $givenID = $id;
            $payload = array(
                'id' => $id
            );

            $result = json_decode($this->api->httpDelete($givenID, $payload), true);
            $this->assertArrayHasKey('status', $result);
            $this->assertEquals($result['status'], 'failed');
        }
    }
?>