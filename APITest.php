<?php

    require_once ('API.php');
    use PHPUnit\Framework\TestCase;

    class APITest extends TestCase
    {
        protected $api;

        protected function setUp(): void
        {
            $this->api = new API();
        }
        
        public function testHttpPost()
        {
            $_SERVER['REQUEST_METHOD'] = 'POST';
            
            $payload = array(
                'first_name' => 'Dustin Jed Wesley',
                'middle_name' => 'Palermo',
                'last_name' => 'Esponilla',
                'contact_number' => '09685502176'
            );
            $result = json_decode($this->api->httpPost($payload), true);
            $this->assertArrayHasKey('status', $result);
            $this->assertEquals($result['status'], 'success');
            $this->assertArrayHasKey('data', $result);

            // gets the ID from the json result
            return $result['data'][0]['id'];
        }

        public function testHttpPostMultiple() {

            $_SERVER['REQUEST_METHOD'] = 'POST';
            
            $payload = array(
                'first_name' => 'Dustin Jed Wesley',
                'middle_name' => 'Palermo',
                'last_name' => 'Esponilla',
                'contact_number' => '09685502176'
            );
            $result = json_decode($this->api->httpPost($payload), true);
            $this->assertArrayHasKey('status', $result);
            $this->assertEquals($result['status'], 'success');
            $this->assertArrayHasKey('data', $result);
            $id1 = $result['data'][0]['id'];
        
            $payload = array(
                'first_name' => 'Number two',
                'middle_name' => 'data number two',
                'last_name' => 'last name ',
                'contact_number' => '09123456789'
            );
            $result = json_decode($this->api->httpPost($payload), true);
            $this->assertArrayHasKey('status', $result);
            $this->assertEquals($result['status'], 'success');
            $this->assertArrayHasKey('data', $result);
            $id2 = $result['data'][0]['id'];

            // gets the IDs from the json result (string format: 1,2)
            return $id1 . ',' . $id2;
        }

        /**
         * @depends testHttpPost
         */
        public function testHttpGet($id) 
        {
            $_SERVER['REQUEST_METHOD'] = 'GET';
            
            // Test 1
            $payload = array(
                'id' => $id
            );

            $result = json_decode($this->api->httpGet($payload), true);
            $this->assertArrayHasKey('status', $result);
            $this->assertEquals($result['status'], 'success');
            $this->assertArrayHasKey('data', $result);
        }
        
        /**
         * @depends testHttpPost
         */
        public function testHttpPut($id) 
        {
            $_SERVER['REQUEST_METHOD'] = 'PUT';

            // Test 1
            $payload = array(
                'id' => $id, 
                'first_name' => 'Dustin Testing Put',
                'middle_name' => 'Put',
                'last_name' => 'Esponilla',
                'contact_number' => '09123456789'
            );

            $result = json_decode($this->api->httpPut($id, $payload), true);
            $this->assertArrayHasKey('status', $result);
            $this->assertEquals($result['status'], 'success');
            $this->assertArrayHasKey('data', $result);

        }

        /**
         * @depends testHttpPost
         */
        public function testHttpDelete($id) 
        {
            $_SERVER['REQUEST_METHOD'] = 'DELETE';

            $payload = array(
                'id' => $id
            );   
            
            $result = json_decode($this->api->httpDelete($id, $payload), true);
            $this->assertArrayHasKey('status', $result);
            $this->assertEquals($result['status'], 'success');
            $this->assertArrayHasKey('data', $result);
        }

        /**
         * @depends testHttpPostMultiple
         */
        public function testHttpDeleteMultiple($ids) {
            $payload = array(
                'id' => $ids
            );

            $result = json_decode($this->api->httpDelete($ids, $payload), true);
            $this->assertArrayHasKey('status', $result);
            $this->assertEquals($result['status'], 'success');
            $this->assertArrayHasKey('data', $result);
        }
    }
?>