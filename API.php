<?php
    
    // /**
    // * Tells the browser to allow code from any origin to access
    // */
    // header("Access-Control-Allow-Origin: *");


    // /**
    // * Tells browsers whether to expose the response to the frontend JavaScript code
    // * when the request's credentials mode (Request.credentials) is include
    // */
    // header("Access-Control-Allow-Credentials: true");


    // /**
    // * Specifies one or more methods allowed when accessing a resource in response to a preflight request
    // */
    // header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");

    // /**
    // * Used in response to a preflight request which includes the Access-Control-Request-Headers to
    // * indicate which HTTP headers can be used during the actual request
    // */
    // header("Access-Control-Allow-Headers: Content-Type");

    require_once('MysqliDb.php');

    class API {

        private $db;
        private $patternForNames = "/^[A-Za-z0-9\s]+$/";
        private $patternForContactNumber = "/^\d{11}$/";

        public function __construct()
        {
            $this->db = new MysqliDb('localhost', 'root', '', 'employee');
        }

        /**
         * HTTP GET Request
         *
         * @param $payload
         */
        public function httpGet($payload)
        {

            // Checks if payload is an array or not
            if (!is_array($payload)) {
                return json_encode(array(
                    'method' => 'GET',
                    'status' => 'failed',
                    'message' => 'Invalid: Payload is not an array.'
                ));
            } 

            try {
                // fetching data from the information table in the database
                foreach ($payload as $attributeName => $value) {
                    $this->db->where($attributeName, $value); 
                }
                $employee = $this->db->get('information');
                
                // checks if the query is a success or not
                if ($this->db->getLastErrno() === 0) {
                    return json_encode(array(
                        'method' => 'GET',
                        'status' => 'success',
                        'data' => $employee
                    ));   
                } else {
                    return json_encode(array(
                        'method' => 'GET',
                        'status' => 'failed',
                        'message' => 'Failed Fetch Request'
                    ));
                }
            } catch (Exception $e) {
                return json_encode(array(
                    'method' => 'GET',
                    'status' => 'failed',
                    'message' => 'Failed Fetch Request'
                ));
            }

        }

        /**
         * HTTP POST Request
         *
         * @param $payload
         */
        public function httpPost($payload)
        {
            // Checks if payload is an array or not 
            if (!is_array($payload)) {
                return json_encode(array(
                    'method' => 'POST',
                    'status' => 'failed',
                    'message' => 'Invalid: Payload is not an array.'
                ));
            } 

            // Checks if the payload is empty
            if (empty($payload)) {
                return json_encode(array(
                    'method' => 'POST',
                    'status' => 'failed',
                    'message' => 'Invalid: Payload is empty.'
                ));
            }

            // checks data validity per attribute in payload
            foreach ($payload as $attributeName => $value) {
                if ($attributeName === 'first_name' || $attributeName === 'middle_name' || $attributeName === 'last_name') {
                    if (!preg_match($this->patternForNames, $value)) {
                        return json_encode(array(
                            'method' => 'POST',
                            'status' => 'failed',
                            'message' => 'Invalid data for ' . $attributeName . ' attribute.'
                        ));
                    }
                } else {
                    if (!preg_match($this->patternForContactNumber, $value) && $attributeName === 'contact_number') {
                        return json_encode(array(
                            'method' => 'POST',
                            'status' => 'failed',
                            'message' => $attributeName . ' must be an 11 digit number.'
                        ));
                    }
                }
            }

            try {
                // inserts data in the database
                $employeeInsert = $this->db->insert('information', $payload);

                // checks if the INSERT query is sucessfully executed or not
                if ($employeeInsert) {
                
                    // finds the ID of the inserted employee 
                    $employeeInsertedId = $this->db->getInsertId();
                    
                    // performs a SELECT query to get ID of the data inserted
                    $this->db->where('id', $employeeInsertedId);
                    $employee = $this->db->get('information');
    
                    // checks if the SELECT query is a success or not
                    if ($this->db->getLastErrno() === 0) {
                        return json_encode(array(
                            'method' => 'POST',
                            'status' => 'success',
                            'data' => $employee
                        ));   
                    } else {
                        return json_encode(array(
                            'method' => 'POST',
                            'status' => 'failed',
                            'message' => 'Invalid: No data found. Failed to Insert Data'
                        ));
                    }
                } else {
                    return json_encode(array(
                        'method' => 'POST',
                        'status' => 'failed',
                        'message' => 'Failed to Insert Data'
                    ));      
                }
            } catch (Exception $e) {
                return json_encode(array(
                    'method' => 'POST',
                    'status' => 'failed',
                    'message' => 'Failed to Insert Data'
                ));      
            }
        }

        /**
         * HTTP PUT Request
         *
         * @param $id
         * @param $payload
         */
        public function httpPut($id, $payload)
        {   
            
            // Checks if the ID is null or empty
            if ($id === null || empty($id)) {
                return json_encode(array(
                    'method' => 'PUT',
                    'status' => 'failed',
                    'message' => 'Invalid: ID is empty or null.'
                ));
            }

            // Checks if the payload is empty
            if (empty($payload)) {
                return json_encode(array(
                    'method' => 'PUT',
                    'status' => 'failed',
                    'message' => 'Invalid: Payload is empty.'
                ));
            }

            if ($id != $payload['id']) {
                return json_encode(array(
                    'method' => 'PUT',
                    'status' => 'failed',
                    'message' => 'Invalid: The ID argument does not match the ID in the payload.'
                ));
            }
            
            // checks data validity per attribute in payload
            foreach ($payload as $attributeName => $value) {
                if ($attributeName === 'first_name' || $attributeName === 'middle_name' || $attributeName === 'last_name') {
                    if (!preg_match($this->patternForNames, $value)) {
                        return json_encode(array(
                            'method' => 'PUT',
                            'status' => 'failed',
                            'message' => 'Invalid data for ' . $attributeName . ' attribute.'
                        ));
                    }
                } else {
                    if (!preg_match($this->patternForContactNumber, $value) && $attributeName === 'contact_number') {
                        return json_encode(array(
                            'method' => 'PUT',
                            'status' => 'failed',
                            'message' => $attributeName . ' must be an 11 digit number.'
                        ));
                    }
                }
            }

            // executing an UPDATE query of an ID
            try {
                $this->db->where('id', $id);
                if ($this->db->update('information', $payload)) {

                    // performs a SELECT query to get ID of the data inserted
                    $this->db->where('id', $id);
                    $employee = $this->db->get('information');
                    
                    if ($this->db->getLastErrno() === 0) {
                        return json_encode(array(
                            'method' => 'PUT',
                            'status' => 'success',
                            'data' => $employee
                        ));   
                    } else {
                        return json_encode(array(
                            'method' => 'PUT',
                            'status' => 'failed',
                            'message' => 'Invalid: No data found. Failed to Update Data.'
                        ));
                    }
                } else {
                    return json_encode(array(
                        'method' => 'PUT',
                        'status' => 'failed',
                        'message' => 'Failed to Update Data'
                    ));
                }
            } catch (Exception $e) {
                return json_encode(array(
                    'method' => 'PUT',
                    'status' => 'failed',
                    'message' => 'Failed to Update Data'
                ));
            }

        }

        /**
         * HTTP DELETE Request
         *
         * @param $id
         * @param $payload
         */
        public function httpDelete($id, $payload)
        {
           
            // Checks if the ID is null or empty
            if ($id === null || empty($id)) {
                return json_encode(array(
                    'method' => 'DELETE',
                    'status' => 'failed',
                    'message' => 'Invalid: ID is empty or null.'
                ));
            }

            // Checks if the payload is empty
            if (empty($payload)) {
                return json_encode(array(
                    'method' => 'DELETE',
                    'status' => 'failed',
                    'message' => 'Invalid: Payload is empty.'
                ));
            }
            
            // executing a DELETE query
            try {
                // deletes single ID
                if (!is_array($payload['id'])) {
                    if ($id != $payload['id']) {
                        return json_encode(array(
                            'method' => 'DELETE',
                            'status' => 'failed',
                            'message' => 'Invalid: The ID argument does not match the ID in the payload.' 
                        ));
                    }
                $this->db->where('id', $id);
                } else { // deletes multiple ID e.g. "id" : [1,2]
                    $idToArray = explode(",", $id); 
                    if ($idToArray != $payload['id']) {
                        return json_encode(array(
                            'method' => 'DELETE',
                            'status' => 'failed',
                            'message' => 'Invalid: The ID argument does not match the ID in the payload.' 
                        ));
                    }
                    $this->db->where('id', $payload['id'], 'IN');
                }

                if ($this->db->delete('information')) {
                    return json_encode(array(
                        'method' => 'DELETE',
                        'status' => 'success',
                        'data' => 'Deleted ID: ' . $id
                    ));
                } else {
                    return json_encode(array(
                        'method' => 'DELETE',
                        'status' => 'failed',
                        'message' => 'Failed to Delete Data'
                    ));
    
                }
            } catch (Exception $e) {
                return json_encode(array(
                    'method' => 'DELETE',
                    'status' => 'failed',
                    'message' => 'Failed to Delete Data'
                ));

            }
        }
        
    }

    // // Identifier if what type of request
    // $request_method = $_SERVER['REQUEST_METHOD']; 

    // // For GET,POST,PUT & DELETE Request
    // if ($request_method === 'GET') {
    //     $received_data = $_GET;
    // } else {
    //     //check if method is PUT or DELETE, and get the ids on URL
    //     if ($request_method === 'PUT' || $request_method === 'DELETE') {
    //         $request_uri = $_SERVER['REQUEST_URI'];

    //         $ids = null;
    //         $exploded_request_uri = array_values(explode("/", $request_uri));

    //         $last_index = count($exploded_request_uri) - 1;
    //         $ids = $exploded_request_uri[$last_index];
    //     }
    // }

    // //payload data
    // $received_data = json_decode(file_get_contents('php://input'), true);

    // $api = new API;

    // $result = null;

    // // Checking if what type of request and designating to specific functions
    // switch ($request_method) {
    //     case 'GET':
    //         $result = $api->httpGet($received_data);
    //         break;
    //     case 'POST':
    //         $result = $api->httpPost($received_data);
    //         break;
    //     case 'PUT':
    //         $result = $api->httpPut($ids, $received_data);
    //         break;
    //     case 'DELETE':
    //         $result = $api->httpDelete($ids, $received_data);
    //         break;
    // }

    // echo $result;  
    
?>
